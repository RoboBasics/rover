//Hollow cylinder (lid)
difference()
{
    cylinder(r=15.5,h=62,$fa=1,$fs=0.5);    //smoothness added in final version 
    translate([0,0,-1])
    cylinder(r=12.5,h=64,$fa=1,$fs=0.5);    //smoothness added in final version
    //Cut cylinder in half
    translate ([-18.5,0,0])
    cube ([37,15.5,62]);
}

//Rims for placing other half
difference()
{
    translate ([-17.5,0,0])
    cube ([35,3,63]);
    translate ([-15.5,0,0])
    cube ([31,5,63]);
}

//Side plate (solid)
translate([0,0,62])
cylinder(r=15.5,h=1,$fa=1,$fs=0.5);         //smoothness added in final version
