//=== Top casing 1 BEGIN ====

//positioning
translate([0,0,0])
{

// Hollow cylinder cut in half
difference()
{
    translate([0,0,0])
    cylinder(r=15.5,h=62);
    translate([0,0,-1])
    cylinder(r=12.5,h=64); //ad smoothness when finished: ,$fa=1,$fs=0.5
    translate ([-18.5,-15.5,0])
    cube ([37,15.5,62]);
// hole for wires
    translate([-19.5,0,50])
    cube([15,17.5,4]);
}

//Side plate with holes
difference()
{
    translate([0,0,0])
    cylinder(r=15.5,h=1); //ad smoothness when finished: ,$fa=1,$fs=0.5
    translate([0,0,-1])
    cylinder(r=3.5,h=3);  //ad smoothness when finished: ,$fa=1,$fs=0.5
    translate([8.5,0,-2])
    cylinder(r=2,h=3);    //ad smoothness when finished: ,$fa=1,$fs=0.5
    translate([-8.5,0,-2])
    cylinder(r=2,h=3);    //ad smoothness when finished: ,$fa=1,$fs=0.5
}

//Stands alongside cylinder
difference()
{
    translate([-15.5,0,0])
    cube([3,15.5,62]);
// hole for wires
    translate([-16.5,0,50])
    cube([5,18.5,4]);
}
translate([12.5,0,0])
cube([3,15.5,62]);

// Close side 
translate([-12.5,8,0])
cube([25,7.5,1]);

//top closure
difference()
{
    translate([-15.5,15.5,0])
    cube([31,3,62]);
// hole for wires
    translate([-16.5,14.5,50])
    cube([10,5,4]);
}
}
//=== Top casing 1 END ====

//=== Leg v-structure BEGIN ===

translate([-15.5,18.5,30.5])
cube([31,20,3]);   
    